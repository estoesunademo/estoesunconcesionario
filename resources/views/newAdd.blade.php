<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            New Post
        </h2>
    </x-slot>
    <div class="flex items-center justify-center">
        <form action="{{route('storeNewAdd')}}" method="post" class="flex flex-col gap-4">
            @csrf
            @method('POST')
            <div class="divider mt-6">Post</div>
            <label class="form-control w-full max-w-xs">
                <div class="label">
                    <span class="label-text">Title</span>
                </div>
                <input type="text" name="title" placeholder="Example" class="input input-bordered w-full max-w-xs"/>
            </label>
            <label class="form-control w-full max-w-xs">
                <div class="label">
                    <span class="label-text">Description</span>
                </div>
                <input type="text" name="desc" placeholder="Write something here"
                       class="input input-bordered w-full max-w-xs"/>
            </label>
            <div class="divider">Cotxe</div>
            <label class="form-control w-full max-w-xs">
                <div class="label">
                    <span class="label-text">Brand</span>
                </div>
                <input type="text" name="brand" placeholder="Example" class="input input-bordered w-full max-w-xs"/>
            </label>
            <label class="form-control w-full max-w-xs">
                <div class="label">
                    <span class="label-text">Model</span>
                </div>
                <input type="text" name="model" placeholder="Example" class="input input-bordered w-full max-w-xs"/>
            </label>
            <label class="form-control w-full max-w-xs">
                <div class="label">
                    <span class="label-text">Year</span>
                </div>
                <input type="number" name="year" placeholder="2000" class="input input-bordered w-full max-w-xs"/>
            </label>
            <label class="form-control w-full max-w-xs">
                <div class="label">
                    <span class="label-text">Price</span>
                </div>
                <input type="number" name="price" placeholder="2000" class="input input-bordered w-full max-w-xs"/>
            </label>
            <label class="form-control w-full max-w-xs">
                <div class="label">
                    <span class="label-text">Km</span>
                </div>
                <input type="number" name="km" placeholder="2000" class="input input-bordered w-full max-w-xs"/>
            </label>
            <label class="form-control w-full max-w-xs">
                <div class="label">
                    <span class="label-text">Image url</span>
                </div>
                <input type="text" name="image" placeholder="https://example.com/example.png" class="input input-bordered w-full max-w-xs"/>
            </label>
            <button class="btn btn-primary w-full max-w-xs mb-6">Add</button>
        </form>
    </div>
</x-app-layout>
