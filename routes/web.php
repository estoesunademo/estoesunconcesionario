<?php

use App\Http\Controllers\CarController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use App\Models\Car;
use App\Models\Post;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

$CarController = CarController::class;
$UserController = UserController::class;
$PostController = PostController::class;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
require __DIR__ . '/auth.php';

//Mostra les ofertes que l'usuari ha guardat.
Route::get('/profile/saved', [$UserController, 'showSaved'])->middleware(['auth', 'verified'])->name("favourites");
Route::get('/posts/{post_id}', [$PostController, 'showPost'])->middleware(['auth', 'verified'])->name("showPost");
// Genera un nou comentari a la publicació seleccionada.
Route::post('/add/newComment', [$PostController, 'addComment'])->middleware(['auth', 'verified'])->name("newComment");;
//Mostra totes les ofertes de cotxes, ordenades de més recent a menys recent.
 Route::get('/', function () {
     $posts = Post::all();
     $postsReverse = $posts->reverse();
     return view('index')->with('posts', $postsReverse);
 })->name('index');
// SI NO ESTÁS REGISTRADO...ES MUY GUARRO PERO FUNCIONA
//Route::get('/', function () {
//    $posts = Post::all();
//    $postsReverse = $posts->reverse();
//    if(Auth::id() != null){
//        return view('index')->with('posts', $postsReverse);
//    } else{
//        return view('index2')->with('posts', $postsReverse);
//    }
//})->name('index');

//Mostra les ofertes que l'usuari ha publicat.
Route::get('/profile/adds', [$UserController, 'showAdds'])->middleware(['auth', 'verified'])->name('myPosts');
//Guarda l'anunci seleccionat a la llista de favorits de l'usuari.
Route::post('/profile/save', [$UserController, 'savePost'])->middleware(['auth', 'verified'])->name('savePost');
//Formulari per crear una nova oferta. Després de crear-la, redirigeix a la pàgina principal.
Route::get('/profile/newAdd', function () {
    return view('newAdd');
})->middleware(['auth', 'verified'])->name('newAdd');
//Guarda la nova oferta creada a la base de dades.
Route::post('/storeNewAdd', [$UserController, 'storeNewAdd'])->middleware(['auth', 'verified'])->name('storeNewAdd');


Route::post('/profile/unsave', [$UserController, 'unsavePost'])->middleware(['auth', 'verified'])->name('unsavePost');


// mi primeraMierda ------>
Route::get('/dashboard', function () {
    return view('dashboard')->with('hola', 54);
})->middleware(['auth', 'verified'])->name('dashboard');


Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});
